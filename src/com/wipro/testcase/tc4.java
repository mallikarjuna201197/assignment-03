package com.wipro.testcase;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.wipro.testbase.base;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class tc4 extends base{
	
	static WebDriver dr;
	@BeforeClass
	public static void start() {
		System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("https://demo.opencart.com");
	}
		
	@AfterClass
	public static  void Stop() {
	dr.close();
	}
	
	@Test
    public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
	 Properties properties=new Properties();
	 properties.load(new FileInputStream("D:\\20111770\\Assignment3\\config\\config.properties"));
	 String url = properties.getProperty("url");
    dr.get(url);
	System.out.println(url);
	Thread.sleep(5000);
	 }
	
	 @Test
	 public void test2()
	 {
		 dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		 dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a")).click();
	 }
	 
	 @Test
	 public void test3() throws BiffException, IOException
	 {
		 File f=new File("D:\\20111770\\Assignment3\\testdata\\login.xls");
			Workbook W=Workbook.getWorkbook(f);
			Sheet s=W.getSheet(0);
			
			int col=s.getColumns();
			String[] str=new String[col];
			
			
				for(int j=0;j<col;j++)
				{
					Cell ce=s.getCell(j, 0);
					str[j]=ce.getContents();
				}
		dr.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys(str[0]);
		dr.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys(str[1]);
	 }
	 
	 @Test
	 public void test4() throws IOException, InterruptedException
	 {
		 dr.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
	        File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
	        FileUtils.copyFile(screenshotFile, new File("D:\\20111770\\Assignment3\\screenshots\\tc4_1.jpg"));
	        Thread.sleep(2500); 
	 }
	 
	 @Test
	 public void test5() throws IOException, InterruptedException
	 {
		 BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt", true));
	        writer.write("\n\n"+dr.findElements(By.tagName("a")).size());
	        writer.close();
	        Thread.sleep(2500);
	 }
	 
	 @Test
	 public void test6() throws IOException, InterruptedException
	 {
		 for(int i=1;i<=8;i++) {
	            dr.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li["+i+"]/a")).click();
	            File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
	            FileUtils.copyFile(screenshotFile, new File("D:\\20111770\\Assignment3\\screenshots\\"+i+".png"));
	        }
	        Thread.sleep(2500);
	 }
	 
	 @Test
	    public void Test7() throws IOException, InterruptedException {
	        dr.findElement(By.linkText("My Account")).click();
	        Thread.sleep(2500);
	    }
	 
	 @Test
	    public void Test8() throws IOException, InterruptedException {
		 dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[5]/a")).click();
	        BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt", true));
	        writer.write("\n"+dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
	        writer.close();
	        Thread.sleep(2500);
	    }

}
