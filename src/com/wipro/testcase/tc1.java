package com.wipro.testcase;

import org.openqa.selenium.chrome.ChromeDriver;

import com.wipro.testbase.base;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class tc1 {
	static WebDriver dr;
	@BeforeClass
	public static void start() {
		System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("https://demo.opencart.com");
	}
		
	@AfterClass
	public static  void Stop() {
	dr.close();
	}
		
	 @Test
     public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
	 Properties properties=new Properties();
	 properties.load(new FileInputStream("D:\\20111770\\Assignment3\\config\\config.properties"));
	 String url = properties.getProperty("url");
     dr.get(url);
	System.out.println(url);
	Thread.sleep(5000);
	 }
		 
     @Test
	public void test2() throws InterruptedException {
	dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
    dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[1]/a")).click();
	Thread.sleep(4000);   
	 }
		
	@Test
		public void test3() throws BiffException, IOException {
		File f=new File("D:\\20111770\\Assignment3\\testdata\\assignment3.xls");
		Workbook W=Workbook.getWorkbook(f);
		Sheet s=W.getSheet(0);
		
		int col=s.getColumns();
		String[] str=new String[col];
		
		
			for(int j=0;j<col;j++)
			{
				Cell ce=s.getCell(j, 0);
				str[j]=ce.getContents();
			}
	dr.findElement(By.xpath("//*[@id=\"input-firstname\"]")).sendKeys(str[0]);
	dr.findElement(By.xpath("//*[@id=\"input-lastname\"]")).sendKeys(str[1]);
	dr.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys(str[2]);
	dr.findElement(By.xpath("//*[@id=\"input-telephone\"]")).sendKeys(str[3]);
	dr.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys(str[4]);
	dr.findElement(By.xpath("//*[@id=\"input-confirm\"]")).sendKeys(str[5]);	
		}
	
		
	@Test
	public void test4() throws IOException, InterruptedException{
    dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[1]")).click();
    BufferedWriter writer = new BufferedWriter(new FileWriter("Policy.txt", true));
	writer.write("\nCheckbox is checked");
    writer.close();
    Thread.sleep(2000);  
    dr.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset[3]/div/div/label[2]/input")).click();
	}
		
   @Test
   public void test5() throws IOException, InterruptedException
   {
	  dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]")).click(); //click on continue
	  BufferedWriter writer = new BufferedWriter(new FileWriter("Policy.txt", true));
		writer.write("\nCongratulations! Your new account has been successfully created! ");
	    writer.close();
	    Thread.sleep(2000);  
   }
   
   @Test
   public void test6()
   {
	   dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
   }
   
   @Test
   public void test7() throws IOException, InterruptedException
   {
	   dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[5]/a")).click();
	   BufferedWriter writer = new BufferedWriter(new FileWriter("Policy.txt", true));
		writer.write("\nsuccessfully logged out ");
	    writer.close();
	    Thread.sleep(2000); 
   }
}




