package com.wipro.testcase;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.wipro.testbase.base;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class tc5 extends base {
	
	static WebDriver dr;
	@BeforeClass
	public static void start()
	{
		System.setProperty("webdriver.chrome.driver","D:\\\\ALM image\\\\Sel\\\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("https://demo.opencart.com");
	}
	
	@AfterClass
	public static void close()
	{
		dr.close();
	}
	
	@Test
	public void test1() throws FileNotFoundException, IOException, InterruptedException
	{
		 Properties properties=new Properties();
		 properties.load(new FileInputStream("D:\\\\20111770\\\\Assignment3\\\\config\\\\config.properties"));
		 String url = properties.getProperty("url");
	    dr.get(url);
		System.out.println(url);
		Thread.sleep(5000);
	}
	
	@Test
    public void Test2() throws InterruptedException {
        dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/i")).click();
        dr.findElement(By.linkText("Login")).click();
        Thread.sleep(2500);
    }
    
    @Test
    public void Test3() throws InterruptedException, BiffException, IOException {
        File f=new File("D:\\20111770\\Assignment3\\testdata\\login.xls");
        Workbook workbook=Workbook.getWorkbook(f);
        Sheet sheet=workbook.getSheet(0);
        int col=sheet.getColumns();
        String[] str=new String[col];
        for(int i=0;i<col;i++) {
            Cell cell=sheet.getCell(i, 0);
            str[i]=cell.getContents();
        }
        dr.findElement(By.name("email")).sendKeys(str[0]);
        dr.findElement(By.name("password")).sendKeys(str[1]);
        Thread.sleep(2500);
    }
    
    @Test
    public void Test4() throws IOException, InterruptedException {
        dr.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
        File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotFile, new File("D:\\20111770\\Assignment3\\screenshots\\tc5_1.jpg"));
        Thread.sleep(2500);
    }
    
    @Test
    public void Test5() throws IOException, InterruptedException {
        dr.findElement(By.linkText("Your Store")).click();
        Thread.sleep(2500);
    }
    
    @Test
    public void Test6() throws IOException, InterruptedException {
        dr.findElement(By.linkText("Brands")).click();
        Thread.sleep(2500);
    }
    
    @Test
    public void Test7() throws IOException, InterruptedException {
        for(int i=1;i<=5;i++) {
            dr.findElement(By.xpath("//*[@id=\"content\"]/div["+i+"]/div/a")).click();
            File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshotFile, new File("D:\\\\20111770\\\\Assignment3\\\\screenshots\\\\tc5_1"+i+".png"));
            dr.navigate().back();
        }
        Thread.sleep(2500);
    }
    
    @Test
    public void Test8() throws IOException, InterruptedException {
        dr.findElement(By.linkText("My Account")).click();
        Thread.sleep(2500);
    }
    
    @Test
    public void Test9() throws IOException, InterruptedException {
        dr.findElement(By.linkText("Logout")).click();
        BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt", true));
        writer.write("\n\n"+dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
        writer.close();
        Thread.sleep(2500);
    }

}
