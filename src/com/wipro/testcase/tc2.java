package com.wipro.testcase;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.wipro.testbase.base;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
public class tc2 extends base{
	
	static WebDriver dr;
	@BeforeClass
	public static void start() {
		System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("https://demo.opencart.com");
	}
		
	@AfterClass
	public static  void Stop() {
	dr.close();
	}
	
	@Test
    public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
	 Properties properties=new Properties();
	 properties.load(new FileInputStream("D:\\\\20111770\\\\Assignment3\\\\config\\\\config.properties"));
	 String url = properties.getProperty("url");
    dr.get(url);
	System.out.println(url);
	Thread.sleep(5000);
	 }
	
	 @Test
	 public void test2()
	 {
		 dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		 dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a")).click();
	 }
	 
	 @Test
	 public void test3() throws BiffException, IOException
	 {
		 File f=new File("D:\\20111770\\Assignment3\\testdata\\login.xls");
			Workbook W=Workbook.getWorkbook(f);
			Sheet s=W.getSheet(0);
			
			int col=s.getColumns();
			String[] str=new String[col];
			
			
				for(int j=0;j<col;j++)
				{
					Cell ce=s.getCell(j, 0);
					str[j]=ce.getContents();
				}
		dr.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys(str[0]);
		dr.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys(str[1]);
	 }
	 
	 @Test
	 public void test4() throws IOException, InterruptedException
	 {
		 dr.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
		 
		 TakesScreenshot scr = (TakesScreenshot)dr;
	        File srcShot = scr.getScreenshotAs(OutputType.FILE);
	        File output = new File("D:\\20111770\\Assignment3\\screenshots\\output.jpg");
	        FileUtils.copyFile(srcShot, output);
	        Thread.sleep(3000);
	 }
	 
	 @Test
	 public void test5() 
	 {
		 dr.findElement(By.xpath("//*[@id=\"content\"]/ul[1]/li[1]/a")).click();
	}
	 
	 @Test
	 public void test6() {
		 dr.findElement(By.xpath("//*[@id=\"input-telephone\"]")).clear();
		 dr.findElement(By.xpath("//*[@id=\"input-telephone\"]")).sendKeys("9876543210");
	 }
	 
	 @Test
	 public void test7() throws IOException, InterruptedException
	 {
		 dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div[2]/input")).click();
		 BufferedWriter writer = new BufferedWriter(new FileWriter("Policy.txt", true));
			writer.write("\nSuccess: Your account has been successfully updated.");
		    writer.close();
		    Thread.sleep(2000);  
		    TakesScreenshot scr = (TakesScreenshot)dr;
	        File srcShot = scr.getScreenshotAs(OutputType.FILE);
	        File output = new File("D:\\20111770\\Assignment3\\screenshots\\output1.jpg");
	        FileUtils.copyFile(srcShot, output);
	        Thread.sleep(3000);
	 }
	 
	 @Test
	 public void test8()
	 {
		 dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
	 }
	 
	 @Test
	 public void test9() throws IOException, InterruptedException
	 {
		 dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click(); 
		//*[@id=\"top-links\"]/ul/li[2]/a
		 dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[5]/a")).click();
		 BufferedWriter writer = new BufferedWriter(new FileWriter("Policy.txt", true));
			writer.write("\nGet successfully logged out ");
		    writer.close();
		    Thread.sleep(2000);  
	 }


}
